package com.homework.project;

import com.homework.project.createData.Data;
import com.homework.project.enums.LectureType;
import com.homework.project.lecture.Lecture;
import com.homework.project.lecture.repository.LectureRepository;
import com.homework.project.lecture.repository.LectureRepositoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class LectureRepositoryImplTest {

    Data data = new Data();
    LectureRepository repository = new LectureRepositoryImpl("src/test/resources/");

    @BeforeEach
    void createStorage() {
        data.createLectureStorage();
    }

    @Test
    void getLecturesByData() {
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(new Lecture("Lecture#2", LocalDateTime.of(2020, 10, 30, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        assertEquals(lectures, repository.getLecturesByData(LocalDate.of(2020, 10, 30)));
    }

    @Test
    void getListOfCurrentTypes() {
        Set<LectureType> lectureTypes = Set.of(LectureType.ONLINE, LectureType.CLASSIC);
        assertEquals(lectureTypes, repository.getListOfCurrentTypes());
    }

    @Test
    void getLectureByType() {
        List<Lecture> lectureList = List.of(
                new Lecture("Lecture#0", LocalDateTime.of(2020, 10, 26, 18, 30),
                        "Robert Martin", "google.com.ua", LectureType.ONLINE),
                new Lecture("Lecture#2", LocalDateTime.of(2020, 10, 30, 18, 30),
                        "Robert Martin", "google.com.ua", LectureType.ONLINE));
        assertEquals(lectureList, repository.getLectureByType(LectureType.ONLINE));
    }

    @Test
    @DisplayName("lecture list grouped by date relative type")
    void getLectureListGroupedByDateRelativeType() {
        Map<Boolean, List<Lecture>> groupedMap = new HashMap<>();
        List<Lecture> lecturesFuture = new ArrayList<>();
        List<Lecture> lecturesPast = new ArrayList<>();
        lecturesFuture.add(new Lecture("Lecture#0", LocalDateTime.of(2020, 10, 26, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        lecturesPast.add(new Lecture("Lecture#2", LocalDateTime.of(2020, 10, 30, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        groupedMap.put(true, lecturesFuture);
        groupedMap.put(false, lecturesPast);
        assertEquals(groupedMap, repository.getLectureMapGroupedByDateRelativeType(
                LocalDateTime.of(2020, 10, 27,18,30), LectureType.ONLINE));
    }
}