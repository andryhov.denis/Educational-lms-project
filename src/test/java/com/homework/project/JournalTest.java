package com.homework.project;

import com.homework.project.createData.Data;
import com.homework.project.enums.LectureType;
import com.homework.project.enums.LiteratureType;
import com.homework.project.lecture.Lecture;
import com.homework.project.literature.Book;
import com.homework.project.literature.ElectronicLiterature;
import com.homework.project.literature.Literature;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class JournalTest {

    Data data = new Data();
    private Journal journal;

    @BeforeEach
    void createStorage() {
        data.createLectureStorage();
        data.createLiteratureStorage();
        journal = new JournalImpl("src/test/resources/");
    }

    @Test
    @DisplayName("verification of receipt of a lectures on several index")
    void getLecturesById() {
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(new Lecture("Lecture#0", LocalDateTime.of(2020, 10, 26, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        lectures.add(new Lecture("Lecture#2", LocalDateTime.of(2020, 10, 30, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        assertEquals(lectures, journal.getLecturesById(0, 2));
    }

    @Test
    @DisplayName("verification of receipt of a lecture by a non-existent index")
    void getLectureByNonExistentId() {
        Lecture lecture = journal.getLectureById(10);
        assertEquals(LectureType.EMPTY, lecture.getType());
    }

    @Test
    @DisplayName("verification of receipt of a lecture by a existing index")
    void getLectureByExistentId() {
        Lecture lecture = new Lecture("Lecture#0", LocalDateTime.of(2020, 10, 26, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE);
        assertEquals(lecture , journal.getLectureById(0));
    }

    @Test
    @DisplayName("getting the full list of Lectures ")
    void getAllLecture() {
        assertEquals(3, journal.getAllLectures().size());
    }

    @Test
    @DisplayName("adding new Lecture")
    void addNewLecture() {
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(new Lecture("Lecture#0", LocalDateTime.of(2020, 10, 26, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        lectures.add(new Lecture("Lecture#3", LocalDateTime.of(2020, 10, 26, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.CLASSIC));
        lectures.add(new Lecture("Lecture#1", LocalDateTime.of(2020, 10, 28, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.CLASSIC));
        lectures.add(new Lecture("Lecture#2", LocalDateTime.of(2020, 10, 30, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        journal.addNewLecture(new Lecture("Lecture#3", LocalDateTime.of(2020, 10, 26, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.CLASSIC));
        assertEquals(lectures, journal.getAllLectures());
    }

    @Test
    @DisplayName("deleting one Lecture")
    void deleteOneLecture() {
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(new Lecture("Lecture#0", LocalDateTime.of(2020, 10, 26, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        lectures.add(new Lecture("Lecture#2", LocalDateTime.of(2020, 10, 30, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        journal.removeLectures(1);
        assertEquals(lectures, journal.getAllLectures());
    }

    @Test
    @DisplayName("deleting several Lectures")
    void deleteMoreLectures() {
        List<Lecture> lectures = new ArrayList<>();
        journal.removeLectures(2, 0, 1, 6);
        assertEquals(lectures, journal.getAllLectures());
    }

    @Test
    @DisplayName("getting list of literature by Lecture id")
    void getLiteraturesByLectureId() {
        List<Literature> literatures = new ArrayList<>();
        literatures.add(new ElectronicLiterature(0,"Lit0", "google1.com.ua", LiteratureType.ELECTRONIC, LocalDate.now()));
        assertEquals(literatures, journal.getLiteraturesByLectureId(0));
    }

    @Test
    @DisplayName("getting empty list of literature by not valid Lecture id")
    void getEmptyLiteraturesListByNotValidLectureId() {
        assertEquals(List.of(), journal.getLiteraturesByLectureId(10));
    }

    @Test
    @DisplayName("deleting literature by index")
    void removeLiterature() {
        List<Literature> literatures = new ArrayList<>();
        literatures.add(new ElectronicLiterature(1,"Lit10", "google2.com.ua", LiteratureType.ELECTRONIC, LocalDate.now()));
        literatures.add(new Book(1,"Clean Code", "R. Martin", LiteratureType.BOOK, LocalDate.now()));
        journal.removeLiterature(0, 2, 5);
        assertEquals(literatures, journal.getLiteraturesByLectureId(1));

    }

    @Test
    @DisplayName("adding new literature")
    void addNewLiterature() {
        List<Literature> literatures = new ArrayList<>();
        literatures.add(new ElectronicLiterature(0,"Lit0", "google1.com.ua", LiteratureType.ELECTRONIC, LocalDate.now()));
        literatures.add(new Book(0,"Clean Code", "Rob. Martin", LiteratureType.BOOK, LocalDate.now()));
        journal.addNewLiterature(new Book(0,"Clean Code", "Rob. Martin", LiteratureType.BOOK, LocalDate.now()));
        assertEquals(literatures, journal.getLiteraturesByLectureId(0));
    }

    @Test
    @DisplayName("parsing string to integer array")
    void parseStringToIntArray() {
        Integer[] integers = new Integer[]{2, 3, 5, 0, 2};
        assertArrayEquals(integers, journal.parseStringToIntArray("2 ffg 3 5 g 0 2"));
    }

    @Test
    void getListOfCurrentLectureTypes() {
        Set<LectureType> lectureTypes = new HashSet<>();
        lectureTypes.add(LectureType.ONLINE);
        lectureTypes.add(LectureType.CLASSIC);
        assertEquals(lectureTypes, journal.getListOfCurrentLectureTypes());
    }

    @Test
    void getLectureByType() {
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(new Lecture("Lecture#1", LocalDateTime.of(2020, 10, 28, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.CLASSIC));
        assertEquals(lectures, journal.getLectureByType(LectureType.CLASSIC));
    }

    @Test
    void getLecturesByData() {
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(new Lecture("Lecture#2", LocalDateTime.of(2020, 10, 30, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        assertEquals(lectures, journal.getLecturesByData(LocalDate.of(2020, 10, 30)));
    }

    @Test
    @DisplayName("save duplicate literature")
    void saveDuplicateOfLiterature() {
        Literature literature = new ElectronicLiterature(1,"Lit10", "google2.com.ua", LiteratureType.ELECTRONIC, LocalDate.now());
        assertFalse(journal.addNewLiterature(literature));
    }

    @Test
    @DisplayName("lecture list grouped by date relative type")
    void getLectureListGroupedByDateRelativeType() {
        Map<Boolean, List<Lecture>> groupedMap = new HashMap<>();
        List<Lecture> lecturesFuture = new ArrayList<>();
        List<Lecture> lecturesPast = new ArrayList<>();
        lecturesFuture.add(new Lecture("Lecture#0", LocalDateTime.of(2020, 10, 26, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        lecturesPast.add(new Lecture("Lecture#2", LocalDateTime.of(2020, 10, 30, 18, 30),
                "Robert Martin", "google.com.ua", LectureType.ONLINE));
        groupedMap.put(true, lecturesFuture);
        groupedMap.put(false, lecturesPast);
        assertEquals(groupedMap, journal.getLectureListGroupedByDateRelativeType(
                LocalDateTime.of(2020, 10, 27,18,30), LectureType.ONLINE));
    }
}