package com.homework.project.service;

import com.homework.project.createData.Data;
import com.homework.project.lecture.repository.LectureRepository;
import com.homework.project.lecture.repository.LectureRepositoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StatisticsServiceTest {


    private final Data data = new Data();
    private StatisticsService service;
    LectureRepository repository;

    @BeforeEach
    void createService() {
        service = new StatisticsServiceImpl();
        data.createLectureStorage();
        repository = new LectureRepositoryImpl("src/test/resources/");
    }

    @Test
    void getTotalLectureDurationTime() {
        assertEquals(420, service.getTotalLectureDurationTime(repository.getAllLectures()));
    }

    @Test
    void getMinLectureDurationTime() {
        assertEquals(90, service.getMinLectureDurationTime(repository.getAllLectures()));
    }

    @Test
    void getMeanLectureDurationTime() {
        assertEquals(140, service.getMeanLectureDurationTime(repository.getAllLectures()));
    }

    @Test
    void getMaxLectureDurationTime() {
        assertEquals(180, service.getMaxLectureDurationTime(repository.getAllLectures()));
    }
}