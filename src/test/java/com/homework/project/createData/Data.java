package com.homework.project.createData;

import com.homework.project.storage.FileStorageLecture;
import com.homework.project.storage.FileStorageLectureDecorator;
import com.homework.project.storage.FileStorageLiterature;
import com.homework.project.enums.LectureType;
import com.homework.project.enums.LiteratureType;
import com.homework.project.lecture.Lecture;
import com.homework.project.literature.Book;
import com.homework.project.literature.ElectronicLiterature;
import com.homework.project.literature.Literature;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Data {

    public void createLectureStorage() {
        FileStorageLectureDecorator storageLectureDecorator = new FileStorageLectureDecorator(
                new FileStorageLecture("src/test/resources/lecture.txt"));

        List<Lecture> lectures;
        Lecture lecture = new Lecture();
        lecture.setId(0);
        lecture.setTitle("Lecture#0");
        lecture.setLocalDateTime(LocalDateTime.of(2020, 10, 26, 18, 30));
        lecture.setLecturer("Robert Martin");
        lecture.setVenue("google.com.ua");
        lecture.setType(LectureType.ONLINE);
        lecture.setDuration(90);

        Lecture lecture1 = new Lecture();
        lecture1.setId(1);
        lecture1.setTitle("Lecture#1");
        lecture1.setLocalDateTime(LocalDateTime.of(2020, 10, 28, 18, 30));
        lecture1.setLecturer("Robert Martin");
        lecture1.setVenue("google.com.ua");
        lecture1.setType(LectureType.CLASSIC);
        lecture1.setDuration(150);

        Lecture lecture2 = new Lecture();
        lecture2.setId(2);
        lecture2.setTitle("Lecture#2");
        lecture2.setLocalDateTime(LocalDateTime.of(2020, 10, 30, 18, 30));
        lecture2.setLecturer("Robert Martin");
        lecture2.setVenue("google.com.ua");
        lecture2.setType(LectureType.ONLINE);
        lecture2.setDuration(180);
        lectures = List.of(lecture, lecture1, lecture2);

        storageLectureDecorator.write(lectures);
    }

    public void createLiteratureStorage() {
        FileStorageLiterature storageLiterature = new FileStorageLiterature("src/test/resources/literature.txt");

        List<Literature> literatureList = new ArrayList<>();

        literatureList.add(new ElectronicLiterature(0,"Lit0", "google1.com.ua", LiteratureType.ELECTRONIC, LocalDate.now()));
        literatureList.add(new ElectronicLiterature(1,"Lit10", "google2.com.ua", LiteratureType.ELECTRONIC, LocalDate.now()));
        literatureList.add(new ElectronicLiterature(1,"Lit11", "google3.com.ua", LiteratureType.ELECTRONIC, LocalDate.now()));
        literatureList.add(new Book(1,"Clean Code", "R. Martin", LiteratureType.BOOK, LocalDate.now()));
        literatureList.add(new ElectronicLiterature(2,"Lit2", "google4.com.ua", LiteratureType.ELECTRONIC, LocalDate.now()));

        storageLiterature.write(literatureList);
    }
}
