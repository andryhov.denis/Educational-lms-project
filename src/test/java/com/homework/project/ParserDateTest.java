package com.homework.project;

import com.homework.project.date.ParserDate;
import com.homework.project.date.ParserDateImpl;
import com.homework.project.exceptions.IncorrectDateTimeEntryException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class ParserDateTest {

    private final ParserDate parserDate = new ParserDateImpl();

    @Test
    @DisplayName("parsing string to LocalDateTime")
    void parsingStringToLocalDateTime() throws IncorrectDateTimeEntryException {
        LocalDateTime localDateTime = LocalDateTime.of(2022, 10, 1, 9, 48);
        String date = "01.10.2022 09:48";
        assertEquals(localDateTime, parserDate.parsingStringToLocalDateTime(date));
    }

    @Test
    @DisplayName("parsing string to LocalDate")
    void parsingStringToLocalDate() throws IncorrectDateTimeEntryException {
        LocalDate localDate = LocalDate.of(2020, 12, 23);
        String date = "23 12 2020";
        assertEquals(localDate, parserDate.parsingStringToLocalDate(date));
    }

    @Test
    @DisplayName("invalid input LocalDateTime and throwing an exception")
    void exceptionParsingStringToLocalDateTime() {
        String date = "011.10e.2022 09d:48";
        assertThrows(IncorrectDateTimeEntryException.class,
                () -> parserDate.parsingStringToLocalDateTime(date));
    }

    @Test
    @DisplayName("invalid input LocalDate and throwing an exception")
    void exceptionParsingStringToLocalDate() {
        String date = "011.10e.23022";
        assertThrows(IncorrectDateTimeEntryException.class,
                () -> parserDate.parsingStringToLocalDate(date));
    }
}