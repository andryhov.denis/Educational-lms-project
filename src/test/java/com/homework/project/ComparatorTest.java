package com.homework.project;

import com.homework.project.comparator.LiteratureDateComparator;
import com.homework.project.enums.LiteratureType;
import com.homework.project.literature.Book;
import com.homework.project.literature.ElectronicLiterature;
import com.homework.project.literature.Literature;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ComparatorTest {

    @Test
    @DisplayName("sort literature by date from new to smaller")
    void sortLiteratureByDateMixedList() {
        List<Literature> literatureTestData = getLiteratureTestDataMixedList();
        List<Literature> literatureResult = new ArrayList<>();
        literatureResult.add(new ElectronicLiterature(1,"Lit157", "google3.com.ua", LiteratureType.ELECTRONIC,
                LocalDate.of(2020, 5, 19)));
        literatureResult.add(new ElectronicLiterature(1,"Lit11", "google3.com.ua", LiteratureType.ELECTRONIC,
                LocalDate.of(2020, 5, 10)));
        literatureResult.add(new ElectronicLiterature(1,"Lit10", "google2.com.ua", LiteratureType.ELECTRONIC,
                LocalDate.of(2020, 5, 4)));
        literatureResult.add(new Book(1,"Clean Code", "R. Martin", LiteratureType.BOOK,
                LocalDate.of(2020, 5, 1)));
        literatureTestData.sort(new LiteratureDateComparator());
        assertEquals(literatureTestData, literatureResult);
    }

    @Test
    void sortLiteratureByDataSameDate() {
        List<Literature> literatureTestDataSameDate = getLiteratureTestDataSameDate();
        List<Literature> literatureTest = new ArrayList<>();
        literatureTest.add(new ElectronicLiterature(1,"Lit222", "google3.com.ua", LiteratureType.ELECTRONIC,
                LocalDate.of(2020, 5, 10)));
        literatureTest.add(new ElectronicLiterature(1,"Lit111", "google3.com.ua", LiteratureType.ELECTRONIC,
                LocalDate.of(2020, 5, 10)));
        literatureTest.sort(new LiteratureDateComparator());
        assertEquals(literatureTestDataSameDate, literatureTest);
    }

    List<Literature> getLiteratureTestDataMixedList() {
        List<Literature> literature = new ArrayList<>();
        literature.add(new ElectronicLiterature(1,"Lit10", "google2.com.ua", LiteratureType.ELECTRONIC,
                LocalDate.of(2020, 5, 4)));
        literature.add(new ElectronicLiterature(1,"Lit11", "google3.com.ua", LiteratureType.ELECTRONIC,
                LocalDate.of(2020, 5, 10)));
        literature.add(new Book(1,"Clean Code", "R. Martin", LiteratureType.BOOK,
                LocalDate.of(2020, 5, 1)));
        literature.add(new ElectronicLiterature(1,"Lit157", "google3.com.ua", LiteratureType.ELECTRONIC,
                LocalDate.of(2020, 5, 19)));
        return literature;
    }

    List<Literature> getLiteratureTestDataSameDate() {
        List<Literature> literatureTest = new ArrayList<>();
        literatureTest.add(new ElectronicLiterature(1,"Lit222", "google3.com.ua", LiteratureType.ELECTRONIC,
                LocalDate.of(2020, 5, 10)));
        literatureTest.add(new ElectronicLiterature(1,"Lit111", "google3.com.ua", LiteratureType.ELECTRONIC,
                LocalDate.of(2020, 5, 10)));
        return literatureTest;
    }
}
