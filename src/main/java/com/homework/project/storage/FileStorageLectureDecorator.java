package com.homework.project.storage;

import com.homework.project.lecture.Lecture;

import java.util.List;

public class FileStorageLectureDecorator implements Storage, AutoCloseable {

    private final FileStorageLecture fileStorage;

    public FileStorageLectureDecorator(FileStorageLecture fileStorage) {
        this.fileStorage = fileStorage;
    }

    @Override
    public List<Lecture> read() {
        return fileStorage.read();
    }

    @Override
    public <T> void write(List<T> list) {
        fileStorage.write(list);
    }

    @Override
    public void close(){
        System.out.println("Closing resources");
    }
}
