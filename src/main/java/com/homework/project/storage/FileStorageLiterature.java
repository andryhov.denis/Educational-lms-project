package com.homework.project.storage;

import com.homework.project.literature.Literature;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileStorageLiterature {

    private final String absolutePath;

    public FileStorageLiterature(String filePath) {
        absolutePath = new File(filePath).getAbsolutePath();
    }

    public List<Literature> read() {
        List<Literature> list = new ArrayList<>();
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(absolutePath))) {
            list = (List<Literature>)objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.getStackTrace();
        }
        return list;
    }

    public <T> void write(List<T> list) {
        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(absolutePath))) {
            objectOutputStream.writeObject(list);
        } catch (IOException e) {
            e.getStackTrace();
        }
    }
}
