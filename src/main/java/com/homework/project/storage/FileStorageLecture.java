package com.homework.project.storage;

import com.homework.project.lecture.Lecture;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileStorageLecture implements Storage {

    private final String absolutePath;

    public FileStorageLecture(String filePath)
    {
        absolutePath = new File(filePath).getAbsolutePath();
    }

    @Override
    public List<Lecture> read() {

        List<Lecture> list = new ArrayList<>();
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(absolutePath))) {
           list = (List<Lecture>)objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.getStackTrace();
        }
        return list;
    }

    public <T> void write(List<T> list) {
        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(absolutePath))) {
            objectOutputStream.writeObject(list);
        } catch (IOException e) {
            e.getStackTrace();
        }
    }
}
