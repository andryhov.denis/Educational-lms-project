package com.homework.project.storage;

import com.homework.project.lecture.Lecture;

import java.io.IOException;
import java.util.List;

public interface Storage {

    List<Lecture> read();

    <T> void write(List<T> list) throws IOException;
}
