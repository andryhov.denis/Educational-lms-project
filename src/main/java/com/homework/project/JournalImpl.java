package com.homework.project;

import com.homework.project.cache.Cache;
import com.homework.project.cache.LectureCache;
import com.homework.project.enums.LectureType;
import com.homework.project.lecture.Lecture;
import com.homework.project.lecture.repository.LectureRepositoryImpl;
import com.homework.project.literature.Literature;
import com.homework.project.literature.repository.LiteratureRepository;
import com.homework.project.literature.repository.LiteratureRepositoryImpl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.List;

public class JournalImpl implements Journal {

    private final LiteratureRepository literatureRepository;
    private final Cache lectureCache;

    public JournalImpl(String path) {
        literatureRepository = new LiteratureRepositoryImpl(path);
        lectureCache = new LectureCache(new LectureRepositoryImpl(path));
    }

    @Override
    public int addNewLecture(Lecture lecture) {
        return lectureCache.saveLecture(lecture);
    }

    @Override
    public List<Lecture> getLecturesByData(LocalDate date) {
        return lectureCache.getLecturesByData(date);
    }

    @Override
    public List<Lecture> getLectureByType(LectureType type) {
        return lectureCache.getLectureByType(type);
    }

    @Override
    public void removeLectures(Integer... idsLectures) {
        StringBuilder errorId = new StringBuilder();
        sortByDesc(idsLectures);

        for (int id : idsLectures) {
            boolean isDelete = lectureCache.deleteLectureByIdAndResources(id);
            if (!isDelete) {
                errorId.append(id).append(" ");
            }
        }
        printMessage(errorId);
    }

    private void sortByDesc(Integer[] ids) {
        Arrays.sort(ids, Collections.reverseOrder());
    }

    @Override
    public Lecture getLectureById(int id) {
        return lectureCache.getLectureById(id);
    }

    @Override
    public List<Lecture> getLecturesById(Integer... idLectures) {
        return lectureCache.getLecturesById(idLectures);
    }

    @Override
    public List<Lecture> getAllLectures() {
        return lectureCache.getAllLectures();
    }

    @Override
    public Set<LectureType> getListOfCurrentLectureTypes() {
        return lectureCache.getListOfCurrentTypes();
    }

    @Override
    public boolean addNewLiterature(Literature literature) {
       return literatureRepository.saveLiterature(literature);
    }

    @Override
    public void removeLiterature(Integer... idLiterature) {
        sortByDesc(idLiterature);
        literatureRepository.deleteLiteratureById(idLiterature);
    }

    @Override
    public List<Literature> getLiteraturesByLectureId(int idLecture) {
       return literatureRepository.getResourcesByLectureId(idLecture);
    }

    public Integer[] parseStringToIntArray(String s) {
        return Arrays.stream(s.split(" "))
                .mapToInt(this::parseToIntWithExceptionHandling)
                .filter(value -> value >=0)
                .boxed()
                .toArray(Integer[]::new);
    }

    @Override
    public Map<Boolean, List<Lecture>> getLectureListGroupedByDateRelativeType(LocalDateTime localDateTime, LectureType type) {
       return lectureCache.getLectureMapGroupedByDateRelativeType(localDateTime, type);
    }

    private int parseToIntWithExceptionHandling(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            System.out.println("Значение " + value + " введено не корректно");
            return -1;
        }
    }

    private void printMessage(StringBuilder errorId) {
        if (!errorId.toString().isEmpty()) {
            System.out.println("Номера лекций, в процессе удаления которых произошла обшибка: " + errorId.toString());
        } else {
            System.out.println("Удаление произведено успешно!");
        }
    }
}