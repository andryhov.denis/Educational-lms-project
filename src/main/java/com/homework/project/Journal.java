package com.homework.project;

import com.homework.project.enums.LectureType;
import com.homework.project.lecture.Lecture;
import com.homework.project.literature.Literature;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface Journal {

    Integer[] parseStringToIntArray(String s);

    List<Literature> getLiteraturesByLectureId(int idLecture);

    void removeLiterature(Integer... idLiterature);

    boolean addNewLiterature(Literature literature);

    List<Lecture> getAllLectures();

    Set<LectureType> getListOfCurrentLectureTypes();

    List<Lecture> getLecturesById(Integer... idLectures);

    Lecture getLectureById(int id);

    void removeLectures(Integer... idsLectures);

    int addNewLecture(Lecture lecture);

    List<Lecture> getLecturesByData(LocalDate date);

    List<Lecture> getLectureByType(LectureType type);

    Map<Boolean, List<Lecture>> getLectureListGroupedByDateRelativeType(LocalDateTime localDateTime, LectureType type);
}
