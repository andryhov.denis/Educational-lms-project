package com.homework.project.lecture;

import com.homework.project.enums.LectureType;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Lecture implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String title;
    private LocalDateTime localDateTime;
    private String lecturer;
    private String venue;
    private LectureType type;
    private int duration;

    public Lecture(String title, LocalDateTime localDateTime, String lecturer, String venue, LectureType type) {
        this.title = title;
        this.localDateTime = localDateTime;
        this.lecturer = lecturer;
        this.venue = venue;
        this.type = type;
    }

    public Lecture() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
         this.localDateTime = localDateTime;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public LectureType getType() {
        return type;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Lecture setType(LectureType type) {
        this.type = type;
        return this;
    }

    @Override
    public String toString() {
        return String.format("%s - Lecture time: %s, " +
                "Lecture topic: %s, " +
                "Speaker: %s, " +
                "location: %s, " +
                "duration: %s minutes",
                getType(),
                localDateTime.format(DateTimeFormatter.ofPattern("d.MM.yyyy H:mm")),
                title, lecturer, venue, duration);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lecture)) return false;
        Lecture lecture = (Lecture) o;
        return  Objects.equals(getTitle(), lecture.getTitle()) &&
                Objects.equals(getLocalDateTime(), lecture.getLocalDateTime()) &&
                Objects.equals(getLecturer(), lecture.getLecturer()) &&
                Objects.equals(getVenue(), lecture.getVenue()) &&
                getType() == lecture.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getLocalDateTime(), getLecturer(), getVenue(), getType());
    }
}
