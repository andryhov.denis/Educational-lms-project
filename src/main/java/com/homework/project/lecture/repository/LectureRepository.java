package com.homework.project.lecture.repository;

import com.homework.project.enums.LectureType;
import com.homework.project.lecture.Lecture;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface LectureRepository {

    int saveLecture(Lecture lecture);

    boolean deleteLectureByIdAndResources(int id);

    Lecture getLectureById(int id);

    List<Lecture> getLecturesById(Integer... ids);

    List<Lecture> getLecturesByData(LocalDate date);

    Set<LectureType> getListOfCurrentTypes();

    List<Lecture> getLectureByType(LectureType type);

    List<Lecture> getAllLectures();

    Map<Boolean, List<Lecture>> getLectureMapGroupedByDateRelativeType(LocalDateTime localDateTime, LectureType type);
}
