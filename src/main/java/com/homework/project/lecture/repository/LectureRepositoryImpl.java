package com.homework.project.lecture.repository;

import com.homework.project.collectors.CustomCollector;
import com.homework.project.storage.FileStorageLecture;
import com.homework.project.storage.FileStorageLectureDecorator;
import com.homework.project.enums.LectureType;
import com.homework.project.lecture.Lecture;
import com.homework.project.literature.repository.LiteratureRepositoryImpl;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class LectureRepositoryImpl implements LectureRepository {

    AtomicInteger currentLectureId;
    FileStorageLectureDecorator fileStorageDecorator;
    LiteratureRepositoryImpl literatureRepository;

    public LectureRepositoryImpl(String path) {
        fileStorageDecorator = new FileStorageLectureDecorator(new FileStorageLecture(path + "lecture.txt"));
        literatureRepository = new LiteratureRepositoryImpl(path);
        this.currentLectureId = new AtomicInteger(calculatingLectureId());
    }

    @Override
    public int saveLecture(Lecture lecture) {
        List<Lecture> allLectures = new ArrayList<>(getAllLectures());
        if (lecture != null) {
            lecture.setId(currentLectureId.incrementAndGet());
            allLectures.add(lecture);
            fileStorageDecorator.write(allLectures);
        }
        return currentLectureId.get();
    }

    @Override
    public boolean deleteLectureByIdAndResources(int id) {
        List<Lecture> collect = getAllLectures().stream()
                .filter(lecture -> lecture.getId() != id)
                .collect(Collectors.toList());
        literatureRepository.removeListByLectureId(id);
        fileStorageDecorator.write(collect);
        return false;
    }

    @Override
    public List<Lecture> getLecturesByData(LocalDate date) {
        return getAllLectures().stream()
                .filter(lecture -> lecture.getLocalDateTime().toLocalDate().equals(date))
                .collect(Collectors.toList());
    }

    @Override
    public Lecture getLectureById(int id) {
       return getAllLectures().stream()
                .filter(lecture -> lecture.getId() == id)
                .findAny()
                .orElse(new Lecture()).setType(LectureType.EMPTY);
    }

    @Override
    public List<Lecture> getLecturesById(Integer[] ids) {
        List<Lecture> lectureList = new ArrayList<>();
        Arrays.stream(ids).forEach(integer -> getAllLectures().stream().
                filter(lecture -> lecture.getId() == integer)
                .forEach(lectureList::add));
        return lectureList;
    }

    @Override
    public Set<LectureType> getListOfCurrentTypes() {
        Set<LectureType> lectureTypes = new HashSet<>();
        List<Lecture> allLectures = getAllLectures();
        if (!allLectures.isEmpty()) {
            allLectures.forEach(lecture -> lectureTypes.add(lecture.getType()));
        }
        return lectureTypes;
    }

    @Override
    public List<Lecture> getLectureByType(LectureType type) {
        List<Lecture> sortLecturesList = new ArrayList<>();
        getAllLectures().stream()
                .filter(lecture -> lecture.getType().equals(type))
                .forEach(sortLecturesList::add);
        return sortLecturesList;
    }

    @Override
    public List<Lecture> getAllLectures() {
        List<Lecture> lectures = fileStorageDecorator.read();
        if(lectures.isEmpty()) return List.of();
        return fileStorageDecorator.read();
    }

    private int calculatingLectureId() {
         return getAllLectures().stream()
                .max(Comparator.comparing(Lecture::getId))
                .orElseThrow(NoSuchElementException::new)
                .getId();
    }

    public Map<Boolean, List<Lecture>> getLectureMapGroupedByDateRelativeType(LocalDateTime localDateTime, LectureType type) {
        List<Lecture> lecturesGroupingByType = getAllLectures()
                .stream()
                .collect(CustomCollector.groupingByLecturesType())
                .get(type);
        return lecturesGroupingByType.stream()
                .collect(Collectors.partitioningBy(lecture -> lecture.getLocalDateTime().isBefore(localDateTime)));
    }
}