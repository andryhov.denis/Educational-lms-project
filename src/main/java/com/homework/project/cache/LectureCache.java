package com.homework.project.cache;

import com.homework.project.collectors.CustomCollector;
import com.homework.project.comparator.LectureDateComparator;
import com.homework.project.enums.LectureType;
import com.homework.project.lecture.Lecture;
import com.homework.project.lecture.repository.LectureRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class LectureCache extends Cache {

    private final List<Lecture> lectures;

    public LectureCache(LectureRepository repository) {
        super(repository);
        lectures = new ArrayList<>(super.getAllLectures());
    }

    @Override
    public List<Lecture> getAllLectures() {
        List<Lecture> collect = List.of();
        if (!lectures.isEmpty()) {
            collect = lectures.stream()
                    .sorted(new LectureDateComparator())
                    .collect(Collectors.toList());
        }
        return collect;
    }

    @Override
    public int saveLecture(Lecture lecture) {
        int id = super.saveLecture(lecture);
        lecture.setId(id);
        lectures.add(lecture);
        return id;
    }

    @Override
    public Set<LectureType> getListOfCurrentTypes() {
        return getMapGroupingByTypeLecture().keySet();
    }

    @Override
    public List<Lecture> getLectureByType(LectureType type) {
       return getMapGroupingByTypeLecture().get(type);
    }

    @Override
    public List<Lecture> getLecturesByData(LocalDate date) {
        List<Lecture> sortLecturesList = new ArrayList<>();
        lectures.stream()
                .filter(lecture -> lecture.getLocalDateTime().toLocalDate().equals(date))
                .forEach(sortLecturesList::add);
        return sortLecturesList;
    }

    @Override
    public boolean deleteLectureByIdAndResources(int id) {
        super.deleteLectureByIdAndResources(id);
        Lecture findLecture = lectures.stream()
                .filter(lecture -> lecture.getId() == id)
                .findAny().orElse(null);

        if(findLecture != null) {
            return lectures.remove(findLecture);
        }
        return false;
    }

    @Override
    public Lecture getLectureById(int id) {
       return lectures.stream()
                .filter(entity -> entity.getId() == id)
                .findFirst()
                .orElse(super.getLectureById(id));
    }

    @Override
    public Map<Boolean, List<Lecture>> getLectureMapGroupedByDateRelativeType(LocalDateTime localDateTime, LectureType type) {
        List<Lecture> lecturesSameType = getMapGroupingByTypeLecture().get(type);
        if(lecturesSameType == null) return Map.of();
        return lecturesSameType.stream()
                .collect(Collectors.partitioningBy(lecture -> lecture.getLocalDateTime().isBefore(localDateTime)));
    }

    private Map<LectureType, List<Lecture>> getMapGroupingByTypeLecture() {
        return lectures.stream().collect(CustomCollector.groupingByLecturesType());
    }
}
