package com.homework.project.cache;

import com.homework.project.enums.LectureType;
import com.homework.project.lecture.Lecture;
import com.homework.project.lecture.repository.LectureRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Cache implements LectureRepository {

    private final LectureRepository repository;

    public Cache(LectureRepository repository) {
        this.repository = repository;
    }

    @Override
    public int saveLecture(Lecture lecture) {
        return repository.saveLecture(lecture);
    }

    @Override
    public boolean deleteLectureByIdAndResources(int id) {
        return repository.deleteLectureByIdAndResources(id);
    }

    @Override
    public Lecture getLectureById(int id) {
        return repository.getLectureById(id);
    }

    @Override
    public List<Lecture> getLecturesById(Integer... ids) {
        return repository.getLecturesById(ids);
    }

    @Override
    public List<Lecture> getLecturesByData(LocalDate date) {
        return repository.getLecturesByData(date);
    }

    @Override
    public Set<LectureType> getListOfCurrentTypes() {
        return repository.getListOfCurrentTypes();
    }

    @Override
    public List<Lecture> getLectureByType(LectureType type) {
        return repository.getLectureByType(type);
    }

    @Override
    public List<Lecture> getAllLectures() {
        return repository.getAllLectures();
    }

    @Override
    public Map<Boolean, List<Lecture>> getLectureMapGroupedByDateRelativeType(LocalDateTime localDateTime, LectureType type) {
        return repository.getLectureMapGroupedByDateRelativeType(localDateTime, type);
    }
}
