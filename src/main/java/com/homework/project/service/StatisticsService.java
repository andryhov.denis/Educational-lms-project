package com.homework.project.service;

import com.homework.project.lecture.Lecture;

import java.util.List;

interface StatisticsService {

    int getTotalLectureDurationTime(List<Lecture> lectureList);

    int getMinLectureDurationTime(List<Lecture> lectureList);

    double getMeanLectureDurationTime(List<Lecture> lectureList);

    int getMaxLectureDurationTime(List<Lecture> lectureList);

    String getTotalStats(List<Lecture> lectureList);
}
