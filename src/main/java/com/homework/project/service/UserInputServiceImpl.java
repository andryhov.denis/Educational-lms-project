package com.homework.project.service;

import com.homework.project.Journal;
import com.homework.project.JournalImpl;
import com.homework.project.date.ParserDate;
import com.homework.project.date.ParserDateImpl;
import com.homework.project.enums.LectureType;
import com.homework.project.enums.LiteratureType;
import com.homework.project.exceptions.AbsentLectureException;
import com.homework.project.exceptions.NonExistentLectureTypeException;
import com.homework.project.lecture.Lecture;
import com.homework.project.literature.Book;
import com.homework.project.literature.ElectronicLiterature;
import com.homework.project.literature.Literature;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class UserInputServiceImpl implements UserInputService {

    private final Journal journal;
    private final BufferedReader reader;
    private final ParserDate parserDate;
    private final StatisticsService statisticsService = new StatisticsServiceImpl();


    public UserInputServiceImpl() {
        journal = new JournalImpl("src/main/resources/");
        reader = new BufferedReader(new InputStreamReader(System.in));
        parserDate = new ParserDateImpl();
    }

    public void deleteItems(int idLecture) throws IOException {
        journal.removeLiterature(journal.parseStringToIntArray(reader.readLine()));
    }


    public void deleteLectures() throws IOException {
        journal.removeLectures(journal.parseStringToIntArray(reader.readLine()));
    }

    public int chooseLecture() throws IOException {
        int id = Integer.parseInt(reader.readLine());
        if(journal.getLectureById(id).getType() != LectureType.EMPTY) {
            return id;
        } else {
            throw new AbsentLectureException("Lectures with this number are not on the list!");
        }
    }

    public void selectSeveralLectures() throws IOException {
        System.out.println("Через пробел введите номера лекций: ");
        List<Lecture> lectures = new ArrayList<>();
        Integer[] idsItems = journal.parseStringToIntArray(reader.readLine());

        if(idsItems.length != 0) {
            lectures = journal.getLecturesById(idsItems);
        }
        printLectures(lectures);
    }

    public void createLecture(LectureType type) throws IOException {
            System.out.println("Введите название лекции:");
            String name = reader.readLine().trim();
            System.out.println("Дата проведения лекции в формате 01.10.2020 08:30");
            String date = reader.readLine().trim();
            System.out.println("Лектор:");
            String lecturer = reader.readLine();
            System.out.println("Место проведения лекции:");
            String venue = reader.readLine();

        if (!name.isEmpty() && !date.isEmpty() && !lecturer.isEmpty() && !venue.isEmpty()) {
            Lecture lecture = new Lecture();
            lecture.setTitle(name);
            lecture.setLocalDateTime(parserDate.parsingStringToLocalDateTime(date));
            lecture.setLecturer(lecturer);
            lecture.setVenue(venue);
            lecture.setType(type);
            addLecture(lecture);
            } else {
            System.out.println("Ни один из атрибутов не может быть пустым!");
        }
    }

    private void addLecture(Lecture lecture) throws IOException {
        int position = journal.addNewLecture(lecture);
        System.out.println("Новая лекция добавлена!");
        System.out.println("Хотите сразу внести литературу для новой лекции?");
        System.out.println("Д(Y)/Н(N)");
        if (isChoice(reader.readLine())) {
            selectTypeLiterature(position);
        }
    }


    public List<Literature> getSubItemsById(int idLecture) {
        return journal.getLiteraturesByLectureId(idLecture);
    }

    public void createLiterature(int idLecture, LiteratureType type) throws IOException {
            System.out.println("Введите название:");
            String title = reader.readLine();
            Literature literature;

            if(!title.isEmpty()) {
                literature = selectionOfTheTypeOfLiteratureCreated(type, title, idLecture);
                boolean isAdding = journal.addNewLiterature(literature);

                if(!isAdding) {
                    System.out.println("Такой ресурс в рамках данной лекции уже существует!");
                }
            } else {
                System.out.println();
                System.out.println("Название не может быть пустым, повторите ввод!");
                System.out.println();
            }
            System.out.println("Продолжить ввод литературы?");
            System.out.println("Д(Y)/Н(N)");
            if(isChoice(reader.readLine())) selectTypeLiterature(idLecture);
    }

    private Literature selectionOfTheTypeOfLiteratureCreated(LiteratureType type, String title, int idLecture) throws IOException {
        Literature literature;
        if(type == LiteratureType.ELECTRONIC) {
            System.out.println("Введите автора или ссылку на ресурс, если это єлектронный источник:");
            String url = reader.readLine();
            literature = new ElectronicLiterature(idLecture, title, url, type, LocalDate.now());
        } else {
            System.out.println("Введите автора:");
            String author = reader.readLine();
            literature = new Book(idLecture, title, author, type, LocalDate.now());
        }
        return literature;
    }

    public Integer selectMenuItem() throws IOException {
        return Integer.parseInt(reader.readLine());
    }

    public void selectTypeLiterature(int idLecture) throws IOException {
        int nMenu;
        do {
            System.out.println("1. Электронная литература");
            System.out.println("2. Книга");
            nMenu = selectMenuItem();
        } while (nMenu < 1 || nMenu > 2);

        if (nMenu == 1) {
            createLiterature(idLecture, LiteratureType.ELECTRONIC);
        } else {
            createLiterature(idLecture, LiteratureType.BOOK);
        }
    }

    private void inputNumberType() throws IOException {
        System.out.println("Что-бы вывести список лекций по типу, введите название типа");
        LectureType lectureType = convertInputToLectureType();
        List<Lecture> lectureByTypeList = journal.getLectureByType(lectureType);

        if(lectureByTypeList.isEmpty()) {
            throw new NonExistentLectureTypeException("Lectures of this type are not on the list");
        }
        printLectures(lectureByTypeList);
        System.out.println(statisticsService.getTotalStats(lectureByTypeList));
    }

    private LectureType convertInputToLectureType() throws IOException {
        String inputType = reader.readLine().toUpperCase();
        LectureType lectureType;

        if(LectureType.contains(inputType)) {
            lectureType = LectureType.valueOf(inputType);
        } else {
            throw new NonExistentLectureTypeException("Introduced lecture type that does not exist");
        }
        return lectureType;
    }

    @Override
    public void inputDate() throws IOException {
        System.out.println("Введите дату в формате дд мм гггг");
        String date = reader.readLine().trim();
        if(!date.isEmpty()) {
            LocalDate localDate = parserDate.parsingStringToLocalDate(date);
            printLecturesByDate(journal.getLecturesByData(localDate));
        } else {
            System.out.println("Пустой ввод, повторите");
        }
    }

    @Override
    public void inputTypeAndDate() throws IOException {
        System.out.println("Enter the lecture type");
        LectureType lectureType = convertInputToLectureType();
        System.out.println("Enter a relative date in format 01.01.1970 23:59");
        LocalDateTime localDateTime = new ParserDateImpl().parsingStringToLocalDateTime(reader.readLine());
        printGroupingLecture(lectureType, localDateTime);
    }

    public void printAllItems() throws IOException {
        List<Lecture> lectures = journal.getAllLectures();
        if(lectures.isEmpty()) {
            System.out.println("Список пуст!");
        }
        System.out.println("Показать сокращенные названия в списке?");
        System.out.println("Д(Y)/Н(N)");

        if(isChoice(reader.readLine())) {
            lectures.forEach(
                    lecture -> System.out.println(lecture.getId() + ". " + lecture.toString().substring(0, 50)));
        } else {
            lectures.forEach(lecture -> System.out.println(lecture.getId() + ". " + lecture.toString()));
        }
    }

    @Override
    public void printLectureTypes() throws IOException {
        journal.getListOfCurrentLectureTypes().forEach(type -> System.out.println(type.name()));
        inputNumberType();
    }

    @Override
    public void printSubItems(int idLecture) {
        List<Literature> literatures = getSubItemsById(idLecture);
        if(literatures.isEmpty()) System.out.println("Список пуст!");

        literatures.forEach(literature -> System.out.println(literature.getId() + ". " + literature.toStringFormat()));
    }

    private void printGroupingLecture(LectureType lectureType, LocalDateTime localDateTime) {
        Map<Boolean, List<Lecture>> groupedMap = journal.getLectureListGroupedByDateRelativeType(localDateTime, lectureType);

        if(!groupedMap.isEmpty()) {
            System.out.println("Upcoming lectures");
            List<Lecture> upcomingLectures = groupedMap.get(false);
            upcomingLectures.forEach(lecture -> System.out.println(lecture.toString()));
            System.out.printf("Total time of future lectures : %s minutes%n",
                    statisticsService.getTotalLectureDurationTime(upcomingLectures));
            System.out.println("Past lectures");
            groupedMap.get(true).forEach(lecture -> System.out.println(lecture.toString()));
        } else {
            System.out.println("the list is empty");
        }
    }

    private void printLecturesByDate(List<Lecture> lectures) {
        printLectures(lectures);
    }

    private void printLectures(List<Lecture> lectures) {
        lectures.forEach(lecture -> System.out.println(lecture.toString()));
    }

    private boolean isChoice(String option) {
        return option.equals("Д") || option.equals("д") || option.equals("Y") || option.equals("y");
    }
}