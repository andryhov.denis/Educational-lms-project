package com.homework.project.service;

import com.homework.project.enums.LectureType;
import com.homework.project.enums.LiteratureType;
import com.homework.project.literature.Literature;

import java.io.IOException;
import java.util.List;

public interface UserInputService {

   Integer selectMenuItem() throws IOException;

   void printAllItems() throws IOException;

   void deleteLectures() throws IOException;

   int chooseLecture() throws IOException;

   void deleteItems(int idLecture) throws IOException;

   void createLecture(LectureType type) throws IOException;

   List<Literature> getSubItemsById(int idLecture);

   void selectSeveralLectures() throws IOException;

   void createLiterature(int idLecture, LiteratureType type) throws IOException;

   void selectTypeLiterature(int idLecture) throws IOException;

   void printLectureTypes() throws IOException;

    void inputDate() throws IOException;

    void inputTypeAndDate() throws IOException;

    void printSubItems(int idLecture);
}
