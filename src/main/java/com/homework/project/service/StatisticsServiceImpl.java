package com.homework.project.service;

import com.homework.project.lecture.Lecture;

import java.util.List;

public class StatisticsServiceImpl implements StatisticsService {

    public int getTotalLectureDurationTime(List<Lecture> lectureList) {
        return lectureList.stream()
                .mapToInt(Lecture::getDuration)
                .reduce(Integer::sum)
                .orElse(0);
    }

    public int getMinLectureDurationTime(List<Lecture> lectureList) {
       return lectureList.stream()
               .mapToInt(Lecture::getDuration)
               .min()
               .orElse(0);
    }

    public double getMeanLectureDurationTime(List<Lecture> lectureList) {
        return lectureList.stream()
                .mapToInt(Lecture::getDuration)
                .average()
                .orElse(0);
    }

    public int getMaxLectureDurationTime(List<Lecture> lectureList) {
        return lectureList.stream()
                .mapToInt(Lecture::getDuration)
                .max()
                .orElse(0);
    }

    public String getTotalStats(List<Lecture> lectureList) {
        return String.format(
          "%nTotal stats%n%n" +
                  "Total duration time: %s%n" +
                  "Minimum lecture duration: %s%n" +
                  "Maximum lecture duration: %s%n" +
                  "Average lecture duration: %s%n" +
                  "Quantity of lectures in current module: %s%n",
                getTotalLectureDurationTime(lectureList),
                getMinLectureDurationTime(lectureList),
                getMaxLectureDurationTime(lectureList),
                getMeanLectureDurationTime(lectureList),
                lectureList.size()
        );
    }
}
