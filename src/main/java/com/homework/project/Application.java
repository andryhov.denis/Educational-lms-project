package com.homework.project;

import com.homework.project.db.PostgresConnectionManager;
import org.flywaydb.core.Flyway;

import javax.sql.DataSource;

public class Application {

    public static void main(String[] args) {
        Flyway flyway = createFlyway(new PostgresConnectionManager().createDataSource());
        flyway.migrate();

        UserMenu userMenu = new UserMenu();
        userMenu.startMainMenu();
    }

    private static Flyway createFlyway(DataSource dataSource) {
        return Flyway.configure()
                .dataSource(dataSource)
                .load();
    }
}
