package com.homework.project.date;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface ParserDate {

    LocalDateTime parsingStringToLocalDateTime(String dateTime);

    LocalDate parsingStringToLocalDate(String date);
}
