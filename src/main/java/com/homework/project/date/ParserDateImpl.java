package com.homework.project.date;

import com.homework.project.exceptions.IncorrectDateTimeEntryException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class ParserDateImpl implements ParserDate {

    public LocalDateTime parsingStringToLocalDateTime(String dateTime) {
        DateTimeFormatter formatterDateTime = DateTimeFormatter.ofPattern("d.MM.yyyy H:mm");
        LocalDateTime dateTimeParse;
        try {
            dateTimeParse = LocalDateTime.parse(dateTime, formatterDateTime);
        } catch (DateTimeParseException e) {
           throw new IncorrectDateTimeEntryException("Incorrect user input of date and time. " +
                   "Please, repeat lecture creation.", e);
        }
        return dateTimeParse;
    }

    public LocalDate parsingStringToLocalDate(String date) {
        DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("dd MM yyyy");
        LocalDate localDateParse;
        try {
            localDateParse = LocalDate.parse(date, formatterDate);
        } catch (DateTimeParseException e) {
            throw new IncorrectDateTimeEntryException("Incorrect user input of date. Please re-enter.", e);
        }
        return localDateParse;
    }
}