package com.homework.project;

import com.homework.project.enums.LectureType;
import com.homework.project.service.UserInputService;
import com.homework.project.service.UserInputServiceImpl;

import java.io.IOException;

public class UserMenu {

    private final UserInputService userInputManager;

    public UserMenu() {
        userInputManager = new UserInputServiceImpl();
    }

    public void startMainMenu() {
        int nMenu = 0;

        while (nMenu != 9) {
            try {
                do {
                    printMainMenu();
                    nMenu = userInputManager.selectMenuItem();
                } while (nMenu < 1 || nMenu > 9);
                selectMenuItem(nMenu);
            } catch (NumberFormatException | IOException e) {
                System.out.println("Не верный ввод, повторите попытку");
            }
        }
    }

    private void selectMenuItem(int nMenu) throws IOException {
        try {
            switch (nMenu) {
                case 1:
                    userInputManager.printAllItems();
                    break;
                case 2:
                    userInputManager.inputDate();
                    break;
                case 3:
                    userInputManager.printLectureTypes();
                    break;
                case 4:
                    userInputManager.inputTypeAndDate();
                    break;
                case 5:
                    userInputManager.selectSeveralLectures();
                    break;
                case 6:
                    selectTypeLectures();
                    break;
                case 7:
                    System.out.println("Введите номера лекции через пробел для удаления:");
                    userInputManager.deleteLectures();
                    break;
                case 8:
                    System.out.println("Введите номер лекции для просмотра:");
                    callInternalMenu(userInputManager.chooseLecture());
                    break;
            }
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }

    private void callInternalMenu(int idLecture) throws IOException {
        int nMenu = 0;

        while (nMenu != 4) {
            do {
                printItemMenu();
                nMenu = userInputManager.selectMenuItem();
            } while (nMenu < 1 || nMenu > 5);

            switch (nMenu) {
                case 1:
                    userInputManager.printSubItems(idLecture);
                    break;
                case 2:
                    userInputManager.selectTypeLiterature(idLecture);
                    System.out.println("Список литературы добавлен!");
                    break;
                case 3:
                    System.out.println("Введите номера через пробел для их удаления:");
                    userInputManager.deleteItems(idLecture);
                    break;
            }
        }
    }

    private void selectTypeLectures() throws IOException {
        int nMenu;
            do {
                System.out.println("1. Онлайн лекция");
                System.out.println("2. Аудитория");
                nMenu = userInputManager.selectMenuItem();
            } while (nMenu < 1 || nMenu > 2);

            if(nMenu == 1) {
                userInputManager.createLecture(LectureType.ONLINE);
            }
            if(nMenu == 2) {
                userInputManager.createLecture(LectureType.CLASSIC);
            }

    }

    private void printMainMenu() {
        System.out.println("        --- Главное меню ---");
        System.out.println("    1. Вывести все доступные лекции (номер и название)");
        System.out.println("    2. Вывести доступные лекции по дате");
        System.out.println("    3. Вывести лекции по типу");
        System.out.println("    4. Вывести лекции по типу и дате проведения");
        System.out.println("    5. Вывести лекции по номерам");
        System.out.println("    6. Добавить новую лекцию");
        System.out.println("    7. Удалить лекции по номерам");
        System.out.println("    8. Выбрать лекцию по номеру");
        System.out.println("    9. Выйти");
        System.out.println("Выберите требуемый пункт:");
    }

    private void printItemMenu() {
        System.out.println("        1. Посмотреть список литературы\n " +
                                   "2. Добавить новую литературу(выбрать тип)\n " +
                                   "3. Удалить литературу \n" +
                                   "4. Вернуться в предыдущее меню");
    }
}
