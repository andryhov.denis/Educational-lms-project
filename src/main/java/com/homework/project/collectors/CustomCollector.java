package com.homework.project.collectors;

import com.homework.project.comparator.LectureDateComparator;
import com.homework.project.enums.LectureType;
import com.homework.project.lecture.Lecture;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;

public class CustomCollector {

    private CustomCollector() {
    }

    public static Collector<Lecture, ?, Map<LectureType, List<Lecture>>> groupingByLecturesType() {
        return Collector.of(
                HashMap::new,
                (lectureTypeListMap, lecture) -> {
                    if(lectureTypeListMap.containsKey(lecture.getType())) {
                        lectureTypeListMap.get(lecture.getType()).add(lecture);
                    } else {
                        var lecturesList = new ArrayList<Lecture>();
                        lecturesList.add(lecture);
                        lectureTypeListMap.put(lecture.getType(), lecturesList);
                    }
                },
                (lectureTypeListMap, lectureTypeListMap2) -> {
                    for (Map.Entry<LectureType, List<Lecture>> entry : lectureTypeListMap2.entrySet()) {
                        LectureType key = entry.getKey();
                        var value = entry.getValue();
                        value.sort(new LectureDateComparator());
                        lectureTypeListMap.put(key, value);
                    }
                    return lectureTypeListMap;
                });
    }
}
