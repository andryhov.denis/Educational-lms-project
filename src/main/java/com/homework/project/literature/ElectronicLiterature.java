package com.homework.project.literature;

import com.homework.project.enums.LiteratureType;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class ElectronicLiterature implements Literature, Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private int lectureId;
    private final String title;
    private final String url;
    private final LocalDate localDate;
    private final LiteratureType type;

    public ElectronicLiterature(int lectureId, String title, String url, LiteratureType type, LocalDate localDate) {
        this.lectureId = lectureId;
        this.title = title;
        this.url = url;
        this.localDate = localDate;
        this.type = type;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getLectureId() {
        return this.lectureId;
    }

    @Override
    public void setLectureId(int id) {
        this.lectureId = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public LocalDate getLocalDate() {
        return this.localDate;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public LiteratureType getType() {
        return type;
    }

    @Override
    public String toStringFormat() {
        return String.format("%s: %s - \"%s\" [%s]", getLocalDate(), getType(), getTitle(), getUrl());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ElectronicLiterature that = (ElectronicLiterature) o;
        return Objects.equals(title, that.title) &&
                Objects.equals(url, that.url) &&
                Objects.equals(localDate, that.localDate) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, url, localDate, type);
    }
}
