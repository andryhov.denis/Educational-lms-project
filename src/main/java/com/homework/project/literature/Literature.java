package com.homework.project.literature;

import com.homework.project.enums.LiteratureType;

import java.time.LocalDate;

public interface Literature {

    int getId();

    void setId(int id);

    int getLectureId();

    void setLectureId(int id);

    String getTitle();

    LocalDate getLocalDate();

    LiteratureType getType();

    String toStringFormat();
}
