package com.homework.project.literature.repository;

import com.homework.project.comparator.LiteratureDateComparator;
import com.homework.project.comparator.LiteratureTypeComparator;
import com.homework.project.storage.FileStorageLiterature;
import com.homework.project.literature.Literature;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class LiteratureRepositoryImpl implements LiteratureRepository {

    FileStorageLiterature fileStorageLiterature;
    AtomicInteger currentLiteratureId;

    public LiteratureRepositoryImpl(String path) {
        this.fileStorageLiterature = new FileStorageLiterature(path + "literature.txt");
        this.currentLiteratureId = new AtomicInteger(calculatingId());
    }

    @Override
    public boolean saveLiterature(Literature literature) {
        List<Literature> allLiterature = getAllLiterature();

        if (!allLiterature.contains(literature)) {
            int newId = currentLiteratureId.incrementAndGet();
            literature.setId(newId);
            allLiterature.add(literature);
            fileStorageLiterature.write(allLiterature);
            return true;
        }
        return false;
    }

    @Override
    public void deleteLiteratureById(Integer[] idLiterature) {
       List<Literature> allLiterature = getAllLiterature();

        Arrays.stream(idLiterature)
                .filter(id -> id >= 0)
                .filter(id -> id < allLiterature.size())
                .forEach(integer -> allLiterature.remove(allLiterature.get(integer)));

        fileStorageLiterature.write(allLiterature);
    }

    @Override
    public List<Literature> getResourcesByLectureId(int idLecture) {
        Comparator<Literature> literatureComparator = new LiteratureTypeComparator().
                thenComparing(new LiteratureDateComparator());
        return getAllLiterature().stream()
                .filter(literature -> literature.getLectureId() == idLecture)
                .sorted(literatureComparator)
                .collect(Collectors.toList());
    }

    private int calculatingId() {
        return Arrays.stream(getAllLiterature().stream()
                .mapToInt(Literature::getId)
                .toArray())
                .max()
                .orElse(0);
    }

    private List<Literature> getAllLiterature() {
        return fileStorageLiterature.read();
    }

    public void removeListByLectureId(int lectureId) {
        List<Literature> collect = getAllLiterature().stream()
                .filter(literature -> literature.getLectureId() != lectureId)
                .collect(Collectors.toList());
        fileStorageLiterature.write(collect);
    }
}
