package com.homework.project.literature.repository;

import com.homework.project.literature.Literature;

import java.util.List;

public interface LiteratureRepository {

    boolean saveLiterature(Literature literature);

    void deleteLiteratureById(Integer[] idLiterature);

    List<Literature> getResourcesByLectureId(int id);
}
