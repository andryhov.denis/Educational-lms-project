package com.homework.project.literature;

import com.homework.project.enums.LiteratureType;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class Book implements Literature, Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private int idLecture;
    private final String title;
    private final String author;
    private final LocalDate localDate;
    private final LiteratureType type;

    public Book(int idLecture, String title, String author, LiteratureType type, LocalDate localDate) {
        this.idLecture = idLecture;
        this.title = title;
        this.author = author;
        this.type = type;
        this.localDate = localDate;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getLectureId() {
        return this.idLecture;
    }

    @Override
    public void setLectureId(int id) {
        this.idLecture = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public LocalDate getLocalDate() {
        return this.localDate;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public LiteratureType getType() {
        return type;
    }

    @Override
    public String toStringFormat()
    {
        return String.format("%s: %s - %s // %s", getLocalDate(), getType(), getAuthor(), getTitle());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(title, book.title) &&
                Objects.equals(author, book.author) &&
                Objects.equals(localDate, book.localDate) &&
                type == book.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, localDate, type);
    }
}
