package com.homework.project.exceptions;

public class NonExistentLectureTypeException extends RuntimeException {
    public NonExistentLectureTypeException(String message) {
        super(message);
    }
}
