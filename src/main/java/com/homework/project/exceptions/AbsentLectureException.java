package com.homework.project.exceptions;

public class AbsentLectureException extends RuntimeException {
    public AbsentLectureException(String message) {
        super(message);
    }
}
