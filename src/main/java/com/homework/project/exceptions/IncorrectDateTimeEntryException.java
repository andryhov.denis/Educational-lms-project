package com.homework.project.exceptions;

public class IncorrectDateTimeEntryException extends RuntimeException {
    public IncorrectDateTimeEntryException(String message, Throwable cause) {
        super(message, cause);
    }
}
