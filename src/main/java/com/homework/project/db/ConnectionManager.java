package com.homework.project.db;

import javax.sql.DataSource;

public interface ConnectionManager {

    DataSource createDataSource();
}
