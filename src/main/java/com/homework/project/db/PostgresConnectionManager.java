package com.homework.project.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

public class PostgresConnectionManager implements ConnectionManager {

    @Override
    public DataSource createDataSource() {
        HikariConfig config = new HikariConfig();

        config.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        config.setSchema("lms");
        config.setUsername("postgres");
        config.setPassword("230588");
        config.setMaximumPoolSize(1);

        return new HikariDataSource(config);
    }
}
