package com.homework.project.enums;

import java.io.Serializable;
import java.util.Arrays;

public enum LectureType implements Serializable {

    ONLINE,
    CLASSIC,
    EMPTY;

    private static final long serialVersionUID = 1L;

    public static boolean contains(String name) {
        return Arrays.stream(LectureType.values()).anyMatch(type -> type.name().equals(name));
    }
}
