package com.homework.project.enums;

import java.io.Serializable;

public enum LiteratureType implements Serializable {
    ELECTRONIC,
    BOOK;

    private static final long serialVersionUID = 1L;
}
