package com.homework.project.comparator;

import com.homework.project.lecture.Lecture;

import java.util.Comparator;

public class LectureDateComparator implements Comparator<Lecture> {
    @Override
    public int compare(Lecture o1, Lecture o2) {
        return o1.getLocalDateTime().compareTo(o2.getLocalDateTime());
    }
}
