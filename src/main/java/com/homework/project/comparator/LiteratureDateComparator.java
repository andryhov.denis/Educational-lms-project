package com.homework.project.comparator;

import com.homework.project.literature.Literature;

import java.util.Comparator;

public class LiteratureDateComparator implements Comparator<Literature> {
    @Override
    public int compare(Literature o1, Literature o2) {
        if(o1.getLocalDate().isBefore(o2.getLocalDate())) {
            return 1;
        } else if(o1.getLocalDate().isAfter(o2.getLocalDate())) {
            return -1;
        } else
            return 0;
    }
}
