package com.homework.project.comparator;

import com.homework.project.literature.Literature;

import java.util.Comparator;

public class LiteratureTypeComparator implements Comparator<Literature> {
    @Override
    public int compare(Literature o1, Literature o2) {
        return o1.getType().compareTo(o2.getType());
    }
}
