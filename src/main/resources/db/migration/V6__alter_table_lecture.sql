alter table lms.lecture
    add constraint lecture_lecture_type_id_fk
        foreign key (lecture_type_id) references lms.lecture_type;

alter table lms.lecture
    add constraint lecture_lecturer_id_fk
        foreign key (lecturer_id) references lms.lecturer;