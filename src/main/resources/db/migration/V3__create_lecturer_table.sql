create table lms.lecturer
(
    id serial not null unique,
    name varchar not null
);

alter table lms.lecturer
    add constraint lecturer_pk
        primary key (id);

