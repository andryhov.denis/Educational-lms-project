create table lms.lecture
(
    id serial not null unique,
    title varchar not null,
    local_date_time date not null,
    lecturer_id int not null,
    venue varchar not null,
    lecture_type_id int not null,
    duration int not null
);

alter table lms.lecture
    add constraint lecture_pk
        primary key (id);

