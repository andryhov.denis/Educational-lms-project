create table lms.literature_type
(
    id serial not null unique,
    type varchar not null
);

alter table lms.literature_type
    add constraint literature_type_pk
        primary key (id);

