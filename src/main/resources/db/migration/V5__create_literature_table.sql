create table lms.literature
(
    id serial not null unique,
    lecture_id int not null,
    title varchar not null,
    author varchar not null,
    local_date_time date not null,
    literature_type_id int not null
);

alter table lms.literature
    add constraint literature_pk
        primary key (id);

