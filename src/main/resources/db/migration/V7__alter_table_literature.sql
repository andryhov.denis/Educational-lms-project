alter table lms.literature
    add constraint literature_lecture_id_fk
        foreign key (lecture_id) references lms.lecture;

alter table lms.literature
    add constraint literature_literature_type_id_fk
        foreign key (literature_type_id) references lms.literature_type;