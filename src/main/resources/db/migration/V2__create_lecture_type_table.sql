create table lms.lecture_type
(
    id serial not null unique,
    type int not null
);

alter table lms.lecture_type
    add constraint lecture_type_pk
        primary key (id);

